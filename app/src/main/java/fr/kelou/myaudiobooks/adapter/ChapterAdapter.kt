package fr.kelou.myaudiobooks.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.kelou.myaudiobooks.BookModel
import fr.kelou.myaudiobooks.MainActivity
import fr.kelou.myaudiobooks.R
import java.io.File

class ChapterAdapter (
    private val context : MainActivity,
    private val bookModel : BookModel
        ): RecyclerView.Adapter<ChapterAdapter.ViewHolder>() {
    private var kelChaptersLines : ArrayList<String> = ArrayList<String>()


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view)
    {
        val chapterTitle: TextView = view.findViewById(R.id.item_chapter_title)
        val chapterDuration: TextView = view.findViewById(R.id.item_chapter_duration)
        val bookCover: ImageView = view.findViewById(R.id.item_book_cover)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val kelChapters = File(bookModel.path + "/chapters.kel")
        if (!kelChapters.exists()) {
            Log.d("ERROR: Unreadable chapter", ""+bookModel.path)
        } else {
            // read chapter per chapter
            val kelChaptersFile = kelChapters.inputStream()
            val lines = ArrayList<String>()
            kelChaptersFile.bufferedReader().forEachLine { lines.add(it) }

            lines.sort()

            this.kelChaptersLines = lines;
        }

        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_chapter, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val title = this.kelChaptersLines[position].split("---".toRegex())[0]
        val duration = this.kelChaptersLines[position].split("---".toRegex())[1].toDouble()

        holder.chapterTitle.text = title

        if (position < bookModel.currentChapter) {
            holder.chapterTitle.setTextColor(Color.parseColor("#99FF75"))
        } else if (position == bookModel.currentChapter) {
            holder.chapterTitle.setTextColor(Color.parseColor("#FFA800"))
        } else {
            holder.chapterTitle.setTextColor(Color.parseColor("#FFFFFF"))
        }



        val seconds = (duration/1000 % 60).toInt()
        var secondsString = ""
        if (seconds < 10) {
            secondsString = "0" + seconds
        } else {
            secondsString = "" + seconds
        }
        val minutes = (duration/1000/60).toInt()
        holder.chapterDuration.text = "" + minutes + ":" + secondsString

        holder.bookCover.setImageURI(bookModel.coverUrl)

        holder.itemView.setOnClickListener {
            context.switchToPlayerFragment(bookModel, position)
        }
    }

    override fun getItemCount(): Int {
        return bookModel.chapters
    }
}