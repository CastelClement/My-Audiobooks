package fr.kelou.myaudiobooks.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.kelou.myaudiobooks.BookModel
import fr.kelou.myaudiobooks.MainActivity
import fr.kelou.myaudiobooks.R

class BookAdapter(
    private val context : MainActivity,
    private val bookList : List<BookModel>,

    ): RecyclerView.Adapter<BookAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val bookCover = view.findViewById<ImageView>(R.id.item_book_cover)
        val bookTitle = view.findViewById<TextView>(R.id.item_book_title)
        val bookDuration = view.findViewById<TextView>(R.id.item_book_duration)
        val bookPercent = view.findViewById<TextView>(R.id.item_book_percent)
        val bookChapters = view.findViewById<TextView>(R.id.item_book_chapters)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_book, parent, false)

        return ViewHolder(view)
    }

    // execute for each BookModel in bookList
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentBook = bookList[position]

        holder.bookCover.setImageURI(currentBook.coverUrl)

        holder.bookTitle.text = currentBook.title

        holder.bookDuration.text = "" + context.millisToStringHMS(currentBook.millisProgress.toDouble()) + " / " + context.millisToStringHMS(currentBook.totalDurationMillis.toDouble())

        val percent = currentBook.millisProgress.toDouble() / currentBook.totalDurationMillis.toDouble() * 100
        holder.bookPercent.text = "" + percent.toInt() + " %"

        holder.bookChapters.text = "" + currentBook.chapters + " chapitres"


        // Change view to chapters when click
        holder.itemView.setOnClickListener{
            context.switchToChapterFragment(currentBook)
        }
    }

    override fun getItemCount(): Int {
        return bookList.size
    }
}