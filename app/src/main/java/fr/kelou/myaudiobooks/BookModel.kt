package fr.kelou.myaudiobooks

import android.net.Uri

class BookModel (
    val title : String = "",
    val totalDurationMillis : Long = 0,
    var millisProgress : Long = 0, // 0 seconds listened
    val chapters : Int = 1,
    var currentChapter : Int = 0,
    val coverUrl : Uri = Uri.EMPTY,
    var path : String = "",
)