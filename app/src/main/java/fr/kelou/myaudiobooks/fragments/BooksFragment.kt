package fr.kelou.myaudiobooks.fragments

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import fr.kelou.myaudiobooks.BookModel
import fr.kelou.myaudiobooks.MainActivity
import fr.kelou.myaudiobooks.R
import fr.kelou.myaudiobooks.adapter.BookAdapter
import java.io.File

class BooksFragment (
    private val context : MainActivity
        ) : Fragment() {
    public val bookList : ArrayList<BookModel> = ArrayList<BookModel>()
    private val adapter : BookAdapter = BookAdapter(this.context, this.bookList)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Read books in register
        val internalBookRegister = File(context?.filesDir?.path + "/books.kel")
        if (!internalBookRegister.exists()) {
            return
        } else {
            val internalBookRegisterStream = internalBookRegister.inputStream()

            val lines = ArrayList<String>()
            internalBookRegisterStream.bufferedReader().forEachLine { lines.add(it) }
            internalBookRegisterStream.close()

            for (l in lines) {
                val bm : BookModel? = this.createBookModelFromPath(l)
                if (bm != null) {
                    this.addBook(bm)
                } else {
                    Toast.makeText(context, "ERROR: Book register: $l", Toast.LENGTH_SHORT)
                }
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val view = inflater?.inflate(R.layout.fragment_books, container, false)

        val bookRecyclerView = view.findViewById<RecyclerView>(R.id.book_recycler_view)
        bookRecyclerView.adapter = this.adapter
        return view
    }

    private fun createBookModelFromPath (path : String) : BookModel?
    {
        val kelData = File(path + "/data.kel")

        val kelDataFile = kelData.inputStream()
        val lines = ArrayList<String>()
        kelDataFile.bufferedReader().forEachLine { lines.add(it) }


        // line 0>0 : title
        // line 2>1 : total duration millis
        // line 4>2 : progress millis
        // line 5>3 : number of chapters
        // line 6>4 : current chapter number
        // line 8>5 : cover path
        // line 9>6 : folder path

        return BookModel(lines[0], lines[1].toLong(), lines[2].toLong(), lines[3].toInt(), lines[4].toInt(), lines[5].toUri(), lines[6])
    }

    public fun addBook (bm : BookModel)
    {
        this.bookList.add(bm)
        this.adapter.notifyItemInserted(this.bookList.size-1)

        println(this.bookList)
    }
}