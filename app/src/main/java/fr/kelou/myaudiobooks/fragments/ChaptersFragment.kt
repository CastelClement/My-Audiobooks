package fr.kelou.myaudiobooks.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import fr.kelou.myaudiobooks.BookModel
import fr.kelou.myaudiobooks.MainActivity
import fr.kelou.myaudiobooks.R
import fr.kelou.myaudiobooks.adapter.ChapterAdapter

class ChaptersFragment (
    private val context : MainActivity,
    public val bookModel : BookModel
    ) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val view = inflater?.inflate(R.layout.fragment_chapters, container, false)

        val chaptersRecyclerView = view.findViewById<RecyclerView>(R.id.chapters_recycler_view)
        chaptersRecyclerView.adapter = ChapterAdapter(context, bookModel)

        return view
    }
}