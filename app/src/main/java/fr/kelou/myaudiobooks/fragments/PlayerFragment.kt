package fr.kelou.myaudiobooks.fragments

import android.graphics.Color
import android.media.MediaPlayer
import android.media.PlaybackParams
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import fr.kelou.myaudiobooks.BookModel
import fr.kelou.myaudiobooks.MainActivity
import fr.kelou.myaudiobooks.R
import java.io.File

class PlayerFragment (
    private val context : MainActivity,
    public val bookModel : BookModel,
    public val chapter : Int
        ): Fragment() {

    init {
        Log.d("PLAYER", "init")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d("PLAYER", "create")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        Log.d("PLAYER", "view")

        val view = inflater?.inflate(R.layout.fragment_player, container, false)


        val playerBookCover = view.findViewById<ImageView>(R.id.player_book_cover)

        val playerClockBtn = view.findViewById<ImageView>(R.id.player_clock)

        val playerBackwardBtn = view.findViewById<ImageView>(R.id.player_backward)
        val playerPlayBtn = view.findViewById<ImageView>(R.id.player_play)
        val playerForwardBtn = view.findViewById<ImageView>(R.id.player_forward)

        val playerSpeedBtn = view.findViewById<TextView>(R.id.player_speed)

        val playerFilename = view.findViewById<TextView>(R.id.player_filename)

        val playerSeekbar = view.findViewById<SeekBar>(R.id.player_seekbar)
        val playerProgressText = view.findViewById<TextView>(R.id.player_progress_text)
        val playerDurationText = view.findViewById<TextView>(R.id.player_duration_text)



        // get audio filename from chapters.kel
        val chaptersKel = File(bookModel.path + "/chapters.kel").inputStream()
        val lines = ArrayList<String>()
        chaptersKel.bufferedReader().forEachLine { lines.add(it) }
        chaptersKel.close()
        val filename = lines[chapter].split("---".toRegex())[0]


        playerBookCover.setImageURI(bookModel.coverUrl)

        playerFilename.text = filename.replace(".mp3", "")

        playerProgressText.text = "0:00"


        val audioFile = File(bookModel.path + "/" + filename)
        val mediaPlayer = MediaPlayer.create(context, audioFile.toUri())

        val pbParams = PlaybackParams()
        pbParams.speed = 1.15f


        playerSpeedBtn.setOnClickListener {
            if (pbParams.speed == 1.15f) {
                pbParams.speed = 1.0f
                playerSpeedBtn.setTextColor(Color.parseColor("#293043"))
            } else {
                pbParams.speed = 1.15f
                playerSpeedBtn.setTextColor(Color.parseColor("#3653D2"))
            }

            if (mediaPlayer.isPlaying) {
                mediaPlayer.playbackParams = pbParams
            }
        }


        var stopAtTheEnd = !context.continueToPlay
        if (stopAtTheEnd) {
            playerClockBtn.setColorFilter(Color.parseColor("#3653D2")) // do stop at the end : BLUE
        } else {
            playerClockBtn.setColorFilter(Color.parseColor("#293043")) // do NOT stop at the end : GREY
        }

        playerClockBtn.setOnClickListener {
            stopAtTheEnd = !stopAtTheEnd
            if (stopAtTheEnd) {
                playerClockBtn.setColorFilter(Color.parseColor("#3653D2")) // do stop at the end : BLUE
            } else {
                playerClockBtn.setColorFilter(Color.parseColor("#293043")) // do NOT stop at the end : GREY
            }
        }


        fun refreshProgressPosition ()
        {
            val seconds = ((mediaPlayer.duration/1000) % 60)
            var secondsString = ""
            if (seconds < 10) secondsString = "0$seconds"
            else secondsString = "" + seconds

            val minutes = (mediaPlayer.duration/1000/60)

            playerDurationText.text = "$minutes:$secondsString"

            playerSeekbar.progress = mediaPlayer.currentPosition
        }


        refreshProgressPosition()

        val handler = Handler()
        val runnable: Runnable = object : Runnable {
            override fun run() {
                playerSeekbar.progress = mediaPlayer.currentPosition
                handler.postDelayed(this, 500)
            }
        }

        playerSeekbar.max = mediaPlayer.duration

        fun play ()
        {
            mediaPlayer.start()
            mediaPlayer.playbackParams = pbParams
            handler.postDelayed(runnable, 0)

            playerPlayBtn.setImageResource(R.drawable.ic_pause)
            playerPlayBtn.setColorFilter(Color.parseColor("#262767"))
        }

        fun pause ()
        {
            mediaPlayer.pause()
            handler.removeCallbacks(runnable)

            playerPlayBtn.setImageResource(R.drawable.ic_play)
            playerPlayBtn.setColorFilter(Color.parseColor("#FDFDFD"))
        }

        playerPlayBtn.setOnClickListener {
            if (mediaPlayer.isPlaying) { // stop player
                pause()
            } else { // start player
                play()
            }
        }

        if (context.continueToPlay) {
            play()
        }

        playerBackwardBtn.setOnClickListener {
            val currentPosition = mediaPlayer.currentPosition

            if (currentPosition > 10000) {
                mediaPlayer.seekTo(currentPosition-10000)
            } else {
                mediaPlayer.seekTo(0)
            }
            if (!mediaPlayer.isPlaying) { // refresh current time text manually
                refreshProgressPosition()
            }
        }
        playerForwardBtn.setOnClickListener {
            val currentPosition = mediaPlayer.currentPosition
            val totalDuration = mediaPlayer.duration

            if (currentPosition+10000 < totalDuration) {
                mediaPlayer.seekTo(currentPosition+10000)
            }
            if (!mediaPlayer.isPlaying) { // refresh current time text manually
                refreshProgressPosition()
            }
        }

        playerSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress)
                }
                val seconds = ((mediaPlayer.currentPosition/1000) % 60)
                var secondsString = ""
                if (seconds < 10) secondsString = "0$seconds"
                else secondsString = "" + seconds

                val minutes = (mediaPlayer.currentPosition/1000/60)

                playerProgressText.text = "$minutes:$secondsString"
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        })


        mediaPlayer.setOnCompletionListener {
            context.continueToPlay = !stopAtTheEnd

            // update currentChapter to next chapter
            if (bookModel.currentChapter == this.chapter) { // if listening to the current chapter
                bookModel.currentChapter = this.chapter+1
                bookModel.millisProgress += mediaPlayer.duration

                val kelDataPrinter = File(bookModel.path+"/data.kel").outputStream().bufferedWriter()
                kelDataPrinter.write("")
                kelDataPrinter.flush() // empty file
                kelDataPrinter.append(bookModel.title + "\n")
                kelDataPrinter.append(bookModel.totalDurationMillis.toString() + "\n")
                kelDataPrinter.append(bookModel.millisProgress.toString() + "\n")
                kelDataPrinter.append(bookModel.chapters.toString() + "\n")
                kelDataPrinter.append(bookModel.currentChapter.toString() + "\n")
                kelDataPrinter.append(bookModel.coverUrl.toString() + "\n")
                kelDataPrinter.append(bookModel.path + "\n")
                kelDataPrinter.flush()
            }

            if (chapter < bookModel.chapters) { // there is at least one chapter remaining in the book
                context.switchToPlayerFragment(this.bookModel, this.chapter + 1)
            }
        }

        return view
    }
}