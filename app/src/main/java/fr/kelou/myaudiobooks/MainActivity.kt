package fr.kelou.myaudiobooks

import android.content.Intent
import android.graphics.Color
import android.media.MediaMetadataRetriever
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.net.toUri
import fr.kelou.myaudiobooks.fragments.BooksFragment
import fr.kelou.myaudiobooks.fragments.ChaptersFragment
import fr.kelou.myaudiobooks.fragments.PlayerFragment
import java.io.File
import java.io.FileOutputStream

class MainActivity : AppCompatActivity() {
    private var booksFragment: BooksFragment = BooksFragment(this)
    private lateinit var chaptersFragment : ChaptersFragment
    private lateinit var playerFragment: PlayerFragment

    public var continueToPlay : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, this.booksFragment)
        transaction.addToBackStack(null)
        transaction.commit()



        val navBooksBtn = findViewById<FrameLayout>(R.id.bottom_nav_books)
        navBooksBtn.setOnClickListener {
            val transaction2 = supportFragmentManager.beginTransaction()
            transaction2.replace(R.id.fragment_container, this.booksFragment)
            transaction2.addToBackStack(null)
            transaction2.commit()

            findViewById<TextView>(R.id.h1_title).text = "My Audiobooks"

            findViewById<ImageView>(R.id.bottom_nav_books_icon).setColorFilter(Color.parseColor("#262767"))
            findViewById<ImageView>(R.id.bottom_nav_chapters_icon).setColorFilter(Color.parseColor("#898A8F"))
        }

        val navChaptersBtn = findViewById<FrameLayout>(R.id.bottom_nav_chapters)
        navChaptersBtn.setOnClickListener {
            try {
                switchToChapterFragment(this.chaptersFragment.bookModel)

            } catch (upae : UninitializedPropertyAccessException) {
                Toast.makeText(this, "Please open a book first", Toast.LENGTH_LONG)
            }
        }

        val navPlayerBtn = findViewById<FrameLayout>(R.id.bottom_nav_play)
        navPlayerBtn.setOnClickListener {
            try {
                switchToPlayerFragment(this.playerFragment.bookModel, this.playerFragment.chapter)

            } catch (upae : UninitializedPropertyAccessException) {
                Toast.makeText(this, "Please open a book first", Toast.LENGTH_LONG)
            }
        }

        val navPlusBtn = findViewById<FrameLayout>(R.id.bottom_nav_add)
        navPlusBtn.setOnClickListener(View.OnClickListener {
            val selectedUri = Uri.parse(Environment.getExternalStorageDirectory().path+"/Audiobooks")
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
            startActivityForResult(Intent.createChooser(intent, "Veuillez choisir un dossier"), 101)
        })
    }


    public fun switchToChapterFragment (bm : BookModel)
    {
        findViewById<TextView>(R.id.h1_title).text = bm.title + " (" + bm.currentChapter + "/" + bm.chapters + ")"
        findViewById<ImageView>(R.id.bottom_nav_books_icon).setColorFilter(Color.parseColor("#898A8F"))
        findViewById<ImageView>(R.id.bottom_nav_play_icon).setColorFilter(Color.parseColor("#898A8F"))
        findViewById<ImageView>(R.id.bottom_nav_chapters_icon).setColorFilter(Color.parseColor("#262767"))

        val transaction = supportFragmentManager.beginTransaction()
        this.chaptersFragment = ChaptersFragment(this, bm)
        transaction.replace(R.id.fragment_container, this.chaptersFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    public fun switchToPlayerFragment (bm : BookModel, chapter : Int)
    {
        findViewById<TextView>(R.id.h1_title).text = bm.title + " [" + (chapter+1) + "]"
        findViewById<ImageView>(R.id.bottom_nav_books_icon).setColorFilter(Color.parseColor("#898A8F"))
        findViewById<ImageView>(R.id.bottom_nav_chapters_icon).setColorFilter(Color.parseColor("#898A8F"))
        findViewById<ImageView>(R.id.bottom_nav_play_icon).setColorFilter(Color.parseColor("#262767"))

        val transaction = supportFragmentManager.beginTransaction()
        this.playerFragment = PlayerFragment(this, bm, chapter)
        transaction.replace(R.id.fragment_container, this.playerFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 101 && resultCode == RESULT_OK) // add a new book
        {
            val bookPath = data?.data?.path?.replace(".*:".toRegex(), Environment.getExternalStorageDirectory().path + "/")
            val bookFolder = File(bookPath)
            val kelData = File("$bookPath/data.kel")

            if (File("$bookPath/infos.txt").exists() && !kelData.exists()) { // file format (infos.txt & data.kel) is ok
                // create BookModel object + initialize directory
                val metadataRetriever = MediaMetadataRetriever()

                // read infos.txt file for title + cover
                val infos = File("$bookPath/infos.txt").inputStream()
                val lines = ArrayList<String>()
                infos.bufferedReader().forEachLine { lines.add(it) }

                var book_title = lines[0]
                val book_cover = bookPath + "/" + lines[1]


                val chapters: Array<File> = bookFolder.listFiles().sortedArray()

                var numberOfChapters: Int = 0
                var durationMillis: Double = 0.0

                val chaptersFile = File("$bookPath/chapters.kel")
                val chaptersFilePrinter = chaptersFile.bufferedWriter()

                // for each chapter
                for (chap in chapters.indices) {
                    // check it's an audio file (.mp3)
                    if (chapters[chap].extension == "mp3") {

                        metadataRetriever.setDataSource(chapters[chap].path)
                        val duration = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                        durationMillis += duration.toDouble()

                        chaptersFilePrinter.append(chapters[chap].name + "---" + duration.toDouble() + "\n")

                        numberOfChapters++
                    }
                }

                chaptersFilePrinter.close()

                val createdBook = BookModel(book_title, durationMillis.toLong(), 0, numberOfChapters, 0, book_cover.toUri(), ""+bookPath)

                val kelDataPrinter = kelData.bufferedWriter()
                kelDataPrinter.append(createdBook.title + "\n")
                kelDataPrinter.append(createdBook.totalDurationMillis.toString() + "\n")
                kelDataPrinter.append(createdBook.millisProgress.toString() + "\n")
                kelDataPrinter.append(createdBook.chapters.toString() + "\n")
                kelDataPrinter.append(createdBook.currentChapter.toString() + "\n")
                kelDataPrinter.append(createdBook.coverUrl.toString() + "\n")
                kelDataPrinter.append(createdBook.path + "\n")
                kelDataPrinter.flush()

                this.booksFragment.addBook(createdBook)

                val fosInternalPath = FileOutputStream(File(filesDir.path + "/books.kel"), true).bufferedWriter()
                fosInternalPath.append(bookPath + "\n")
                fosInternalPath.flush()
                fosInternalPath.close()
            }
        } // END OF RESULT CODE 101 (Add a book)
    }

    public fun millisToStringHMS(d : Double) : String
    {
        var minutes : String = (((d / 1000) / 60) % 60).toInt().toString()
        val hours : String = (((d / 1000) / 60) / 60).toInt().toString()

        if (minutes.toInt() < 10) {
            minutes = "0"+minutes
        }

        return "$hours h $minutes"
    }

    public fun millisToStringMS(d : Double) : String
    {
        var seconds : String = ((d/1000) % 60).toInt().toString()
        var minutes : String = ((d / 1000) / 60).toInt().toString()

        if (seconds.toInt() < 10) {
            seconds = "0"+seconds
        }

        return "$minutes:$seconds"
    }
}