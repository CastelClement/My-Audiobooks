# My Audiobooks

[**Télécharger l'APK**](./app/release/app-release.apk)

[Présentation sur le blog](https://kelou.fr/post/my-audiobooks-presentation)

[Présentation technique de l'application & processus de développement](https://kelou.fr/post/my-audiobooks-presentation-technique)

## Présentation

MyAudiobooks est une application de lecture de livres audio.

![interfaces graphiques](./resources/readme/MyAudiobooks.png)

## Fonctionnalités

 - Importer ses livres audio
 - Suivi de la lecture
 - Marque-page
 - Vitesses x1.0 et x1.15
 - Play/pause
 - Arrêt automatique à la fin du fichier/Enchainement automatique des fichiers